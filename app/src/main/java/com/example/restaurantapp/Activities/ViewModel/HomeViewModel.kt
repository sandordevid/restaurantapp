package com.example.restaurantapp.Activities.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.restaurantapp.Activities.API.RecyclerList
import com.example.restaurantapp.Activities.API.RetroInstance
import com.example.restaurantapp.Activities.API.RetroService
import com.example.restaurantapp.Activities.API.apiRestaurant
import com.example.restaurantapp.Activities.Model.Restaurant
import kotlinx.coroutines.Dispatchers
//import com.example.restaurantapp.Activities.Repository.Repository
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.create

/*
class HomeViewModel (private val repository: Repository): ViewModel(){

    val myResponse: MutableLiveData<Restaurant> = MutableLiveData()
    fun getRestaurant()
    {
        viewModelScope.launch {
            val response: Restaurant = repository.getOneRes()
            myResponse.value = response
        }
    }
}

 */


class HomeViewModel : ViewModel(){
    lateinit var recyclerListLiveData: MutableLiveData<RecyclerList>

    init {
        recyclerListLiveData = MutableLiveData()
    }

    fun getRecyclerListObserver(): MutableLiveData<RecyclerList>{
        return recyclerListLiveData
    }

    fun makeApiCall(){
        viewModelScope.launch(Dispatchers.IO){
            val retroInstance = RetroInstance.getRetroInstance().create(RetroService::class.java)
            val response = retroInstance.getRestaurants("1","100")
            //val response = retroInstance.getRestaurantsByCountry("RO")
            Log.i("test",response.restaurants.toString())
            recyclerListLiveData.postValue(response)
        }
    }
}

