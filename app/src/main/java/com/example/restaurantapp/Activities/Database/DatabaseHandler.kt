package com.example.restaurantapp.Activities.Database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.widget.Toast
import com.example.restaurantapp.Activities.Model.Profile

class DatabaseHandler (var context: Context) :
    SQLiteOpenHelper(context,DATABASE_NAME,null,1) {
    companion object{
        private const val DATABASE_NAME = "profile.db"
        private const val TABLE_NAME="Profile"
        private const val COL_NAME = "name"
        private const val COL_ADDRESS = "address"
        private const val COL_EMAIL = "email"
        private const val COL_PNUMBER = "pnumber"
        private const val COL_ID = "id"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable =  "CREATE TABLE " + TABLE_NAME + "(" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_NAME + " TEXT," + COL_ADDRESS + " TEXT," + COL_EMAIL + " TEXT," +
                COL_PNUMBER + " TEXT " + ")"
    db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun readData() : Profile{
        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME + "WHERE ROWNUM = 1"
        val result = db.rawQuery(query,null)
        var profile = Profile()

            profile.id = result.getString(result.getColumnIndex(COL_ID)).toInt()
            profile.name = result.getString(result.getColumnIndex(COL_NAME))
            profile.address = result.getString(result.getColumnIndex(COL_ADDRESS))
            profile.email = result.getString(result.getColumnIndex(COL_EMAIL))
            profile.phoneNumber = result.getString(result.getColumnIndex(COL_PNUMBER))
        Log.d("profile",profile.toString())
        result.close()
        db.close()
        return profile
        }

    fun deleteData(){

    }

    fun insertData(profile: Profile): Long{
        val db = this.writableDatabase
        db.execSQL("delete from "+ TABLE_NAME);
        var cv = ContentValues()
        cv.put(COL_NAME,profile.name)
        cv.put(COL_ADDRESS,profile.address)
        cv.put(COL_EMAIL,profile.email)
        cv.put(COL_PNUMBER, profile.phoneNumber)
        val result = db.insert(TABLE_NAME,null,cv)
        if (result == -1.toLong())
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()
        db.close()
        return result
    }

    fun getAllProfiles(): ArrayList<Profile>{
        val profileList: ArrayList<Profile> = ArrayList()
        val selectQuery = "SELECT * FROM " + TABLE_NAME
        val db = this.readableDatabase

        val cursor: Cursor?

        try{
            cursor = db.rawQuery(selectQuery,null)
        }catch(e: Exception){
            e.printStackTrace()
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var id: Int
        var name: String
        var email: String
        var address: String
        var pnumber: String

        if (cursor.moveToFirst()){
            do{
                id = cursor.getInt(cursor.getColumnIndex("id"))
                name = cursor.getString(cursor.getColumnIndex("name"))
                address = cursor.getString(cursor.getColumnIndex("address"))
                email = cursor.getString(cursor.getColumnIndex("email"))
                pnumber = cursor.getString(cursor.getColumnIndex("pnumber"))
                val ple = Profile(name = name, address = address, email = email, phoneNumber = pnumber)
                profileList.add(ple)
            }while (cursor.moveToNext())
        }
        return profileList
    }
}