package com.example.restaurantapp.Activities.Adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.restaurantapp.Activities.API.RecyclerList
import com.example.restaurantapp.Activities.API.apiRestaurant
import com.example.restaurantapp.Activities.Fragments.DetailFragment
import com.example.restaurantapp.R
import com.google.android.material.progressindicator.DeterminateDrawable
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycler_list_row.view.*

class RecyclerViewAdapter(): RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>(){

    var items = ArrayList<apiRestaurant>()
        //private lateinit var mListener: OnItemClickListener

    fun setUpdatedData(items: ArrayList<apiRestaurant>)
    {
        this.items = items
        notifyDataSetChanged()
    }

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val imageThumb = view.imageThumb
        val resName = view.resName
        val resCity = view.resCity

        fun bind(data: apiRestaurant) {
            resName.text = data.name
            resCity.text = data.city
            Log.d("----------------------------------------------------------", data.name)

            val url = data.image_url
            Glide.with(imageThumb)
                .load(url)
                .into(imageThumb)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_list_row,parent,false)
        d("tester","test")
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val restaurant: apiRestaurant = items[position]
        holder.bind(items[position])

        holder.itemView.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                val activity=v!!.context as AppCompatActivity
                activity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container,DetailFragment
                    .newInstance(restaurant))
                    .addToBackStack("fragmentStack")
                    .commit()
            }

        })
    }


}