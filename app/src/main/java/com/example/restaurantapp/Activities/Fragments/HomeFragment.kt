package com.example.restaurantapp.Activities.Fragments


import android.os.Bundle
import android.util.Log
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.executor.GlideExecutor.UncaughtThrowableStrategy.LOG
import com.example.restaurantapp.Activities.API.*
import com.example.restaurantapp.Activities.Adapter.RecyclerViewAdapter
//import com.example.restaurantapp.Activities.Factory.HomeViewModelFactory
import com.example.restaurantapp.Activities.HomeActivity
import com.example.restaurantapp.Activities.Model.Restaurant
import com.example.restaurantapp.Activities.ViewModel.HomeViewModel
//import com.example.restaurantapp.Activities.Repository.Repository
//import com.example.restaurantapp.Activities.ViewModel.HomeViewModel

import com.example.restaurantapp.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory


private const val BASE_URL = "https://ratpark-api.imok.space/"
/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    private lateinit var recyclerAdapter: RecyclerViewAdapter

    //private lateinit var  viewModel: HomeViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_home, container, false)
        /*
        val repository = Repository()
        val viewModelFactory = HomeViewModelFactory(repository)
        viewModel = ViewModelProvider(this,viewModelFactory).get(HomeViewModel::class.java)
        viewModel.getRestaurant()
        viewModel.myResponse.observe(this, Observer { response ->
            Log.d("Response", response.id)
            Log.d("Response", response.name)
            Log.d("Response", response.address)
            Log.d("Response", response.city)
            Log.d("Response", response.state)
            Log.d("Response", response.area)
            Log.d("Response", response.postal_code)
            Log.d("Response", response.country)
            Log.d("Response", response.phone)
        })
*/
        //EGY MÁSIK PRÓBÁLKOZÁS
        /*

        val api = RetroInstance.getRetroInstance().create(RetroService::class.java)
        api.searchRestaurants("1").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                Log.d("tester", "onResponse $response")
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Log.d("tester", "onFailure $t")
            }

        })

*/
        initViewModel(v)
        initViewModel()

        return v;
    }



    private fun initViewModel(v: View){

        val recyclerView = v.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val deco = DividerItemDecoration(activity,DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(deco)

        recyclerAdapter = RecyclerViewAdapter()
        recyclerView.adapter = recyclerAdapter

        /*
        recyclerAdapter.setOnItemClickListener(object: RecyclerViewAdapter.OnItemClickListener{
            override fun setOnClickListener(pos: Int) {
                fragmentManager!!
                    .beginTransaction()
                    .replace(R.id.container,DetailFragment()).addToBackStack(null).commit()

            }

        })

         */
        d("tester","decoration")
    }

    private fun initViewModel(){
        val viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        viewModel.getRecyclerListObserver().observe(this, Observer<RecyclerList> {
            if (it != null){
                recyclerAdapter.setUpdatedData(it.restaurants)
            }else{
                Toast.makeText(activity, "Error in getting data",Toast.LENGTH_SHORT)
            }
        })
        viewModel.makeApiCall()
    }


}


