package com.example.restaurantapp.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import com.example.restaurantapp.R
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar?.hide()
        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottom_navigation_bar)
        val navController = Navigation.findNavController(this, R.id.fragment_container)

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.homeFragment,R.id.restaurantFragment,R.id.favoriteFragment,R.id.profileFragment))
        setupActionBarWithNavController(navController,appBarConfiguration)
        val fragmentManager = supportFragmentManager
        bottomNavigationView.setupWithNavController(navController)


    }
}
