package com.example.restaurantapp.Activities.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.restaurantapp.Activities.API.apiRestaurant
import com.example.restaurantapp.Activities.HomeActivity
import com.example.restaurantapp.Activities.Model.Restaurant
import com.example.restaurantapp.R
import kotlinx.android.synthetic.main.fragment_detail.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var name: TextView
    private lateinit var address: TextView
    private lateinit var city: TextView
    private lateinit var country: TextView
    private lateinit var price: TextView
    private lateinit var phone: TextView
    private lateinit var url: ImageView
    private lateinit var button: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_detail, container, false)

        name = v.findViewById<TextView>(R.id.name)
        address = v.findViewById<TextView>(R.id.address)
        city = v.findViewById<TextView>(R.id.city)
        country = v.findViewById<TextView>(R.id.country)
        price = v.findViewById<TextView>(R.id.price)
        phone = v.findViewById<TextView>(R.id.pnumber)
        url = v.findViewById<ImageView>(R.id.imageView)
        button = v.findViewById<Button>(R.id.backButton)

        val restaurant: apiRestaurant
        restaurant = arguments?.get(key) as apiRestaurant
        name.setText(restaurant.name)
        address.setText(restaurant.address)
        city.setText(restaurant.city)
        country.setText(restaurant.country)
        price.setText(restaurant.price.toString())
        phone.setText(restaurant.phone)



        Glide.with(url)
            .load(restaurant.image_url)
            .into(url)


        button.setOnClickListener(View.OnClickListener{
            val intent = Intent(this.context, HomeActivity::class.java)
            startActivity(intent)
        })

        return v
    }

    companion object {
        private val key = "myFragment_caught"

        fun newInstance(restaurant: apiRestaurant): DetailFragment {
            val args: Bundle = Bundle()
            args.putSerializable(key, restaurant)
            val fragment = DetailFragment()
            fragment.arguments = args
            return fragment
        }
    }
}