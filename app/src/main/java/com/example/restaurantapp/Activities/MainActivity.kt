package com.example.restaurantapp.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import com.example.restaurantapp.R

class MainActivity : AppCompatActivity() {
    private var handler = Handler()
    var progressStatus: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        val resources = resources
        val drawable = resources.getDrawable(R.drawable.circular_progressbar)
        val progressBar: ProgressBar = findViewById(R.id.progressBar)
        progressBar.progress = 0
        progressBar.secondaryProgress = 100
        progressBar.max = 100
        progressBar.progressDrawable = drawable
        Thread(Runnable {
            while (progressStatus < 100) {
                progressStatus += 2
                handler.post {
                    progressBar.progress = progressStatus
                }
                try {
                    Thread.sleep(50)
                }
                catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }).start()

        Handler().postDelayed({
            val intent = Intent(this@MainActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }, 3000)
    }
}
