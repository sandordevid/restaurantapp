package com.example.restaurantapp.Activities.API

import com.example.restaurantapp.Activities.Model.Restaurant
import com.example.restaurantapp.Activities.Model.Restaurants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetroService {
    @GET("restaurants")
    suspend fun getRestaurantByName(@Query("name") name: String): apiRestaurant

    @GET("restaurants")
    suspend fun getRestaurants(
        @Query("page") page: String,
        @Query("per_page") per_page: String
    ): RecyclerList

    @GET("restaurants")
    suspend fun getRestaurantsByCountry(@Query("country") country: String): RecyclerList
/*
    @GET("restaurants")
    fun searchRestaurants(
        @Query("page") searchTerm: String
    ): Call<Any>
*/
    @GET("cities")
    suspend fun getCities() : Cities
}