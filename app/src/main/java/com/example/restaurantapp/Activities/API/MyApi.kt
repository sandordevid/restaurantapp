package com.example.restaurantapp.Activities.API

import retrofit2.Call
import com.example.restaurantapp.Activities.Model.Cities
import com.example.restaurantapp.Activities.Model.Countries
import com.example.restaurantapp.Activities.Model.Restaurant
import com.example.restaurantapp.Activities.Model.Restaurants
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApi {
    @GET("cities")
    suspend fun getCities(): Cities

    @GET("countries")
    suspend fun getCountries(): Countries

    @GET("restaurants")
    suspend fun getRestaurants(): Call<apiRestaurant>


}