package com.example.restaurantapp.Activities.Model

data class Restaurants (
    val total_entries: String,
    val page: String,
    val restaurants: List<Restaurant>)