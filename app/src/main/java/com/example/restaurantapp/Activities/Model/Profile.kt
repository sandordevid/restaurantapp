package com.example.restaurantapp.Activities.Model

class Profile {
    var id : Int = 0
    var name : String = ""
    var address : String = ""
    var email : String = ""
    var phoneNumber : String = ""

    constructor(name : String, address: String, email : String, phoneNumber:String){
        this.name = name
        this.address = address
        this.email = email
        this.phoneNumber = phoneNumber
    }
    constructor(){
    }
}