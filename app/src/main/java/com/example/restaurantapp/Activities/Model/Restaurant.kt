package com.example.restaurantapp.Activities.Model

import android.os.Parcelable
import com.squareup.moshi.Json

data class Restaurant (val id: String,
                       val name: String,
                       val address: String,
                       val city: String,
                       val state: String,
                       val area: String,
                       val postal_code: String,
                       val country: String,
                       val phone: String,
                       val lat: Float,
                       val lng: Float,
                       val price: String,
                       val reserve_url: String,
                       val mobile_reserve_url: String,
                       @Json(name="image_url") val imgSrcUrl: String) {
    override fun toString(): String {
        return "${name}\nPrice: ${price}$\n${city}, ${state}, ${country}\n\nAddress\n${address}\n\n${area}\n\nPostal code\n${postal_code}"
    }
}
