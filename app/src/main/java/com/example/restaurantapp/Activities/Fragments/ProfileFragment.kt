package com.example.restaurantapp.Activities.Fragments




import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import com.example.restaurantapp.Activities.Database.DatabaseHandler
import com.example.restaurantapp.Activities.HomeActivity
import com.example.restaurantapp.Activities.Model.Profile
import com.example.restaurantapp.R
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_profile.*


/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {
    //private lateinit var btnAdd: AppCompatButton
    private lateinit var btnUpdate: AppCompatButton
    private lateinit var btnAddAvatar: AppCompatButton
    private lateinit var btnUpdateAvatar: AppCompatButton
    private lateinit var btnLoad: AppCompatButton
    private lateinit var name: TextInputEditText
    private lateinit var address: TextInputEditText
    private lateinit var email: TextInputEditText
    private lateinit var phoneNumber: TextInputEditText

    private lateinit var databaseHelper: DatabaseHandler

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_profile, container, false)
        databaseHelper = DatabaseHandler(v.context)
        initViews(v)
        initListeners(v)

        return v;
    }



    private fun initViews(v: View) {
        //btnAdd = v.findViewById<AppCompatButton>(R.id.appCompatButtonAddData)
        btnUpdate = v.findViewById<AppCompatButton>(R.id.appCompatButtonChangeData)
        btnAddAvatar = v.findViewById<AppCompatButton>(R.id.appCompatButtonUploadAvatar)
        btnUpdateAvatar = v.findViewById<AppCompatButton>(R.id.appCompatButtonChangeAvatar)
        btnLoad = v.findViewById<AppCompatButton>(R.id.appCompatButtonLoad)

        name = v.findViewById<TextInputEditText>(R.id.textInputEditTextName)
        address = v.findViewById<TextInputEditText>(R.id.textInputEditTextAddress)
        email = v.findViewById<TextInputEditText>(R.id.textInputEditEmail)
        phoneNumber = v.findViewById<TextInputEditText>(R.id.textInputEditTextPhoneNumber)


    }

    private fun initListeners(v: View) {
        /*
        btnAdd.setOnClickListener {
            addProfile(v)
        }
        */
        btnLoad.setOnClickListener(){
            databaseSizeTester()
        }

        btnUpdate.setOnClickListener {
            addProfile(v)
            clearFields()
            //databaseSizeTester()
        }
        btnAddAvatar.setOnClickListener {
            databaseSizeTester()
        }
        btnUpdateAvatar.setOnClickListener {

        }

    }

    private fun loadProfile(v: View){
        var db = DatabaseHandler(v.context)
        var data = db.readData()
        name.setText(data.name.toString())
        address.setText(data.address.toString())
        email.setText(data.email.toString())
        phoneNumber.setText(data.phoneNumber.toString())
    }

    private fun clearFields(){
        name.setText("")
        address.setText("")
        email.setText("")
        phoneNumber.setText("")
    }

    private fun addProfile(v: View) {
        if (name.text.toString().isEmpty()) {
            name.error = "Please Enter your Name"
            name.requestFocus()
        }

        else if (address.text.toString().isEmpty()) {
            address.error = "Please Enter your Address"
            address.requestFocus()
        }

        else if (email.text.toString().isEmpty()) {
            email.error = "Please Enter your Email"
            email.requestFocus()
        }

        else if (phoneNumber.text.toString().isEmpty()) {
            phoneNumber.error = "Please Enter your Phone Number"
            phoneNumber.requestFocus()
        }
        else
        {
            var profile = Profile(name.text.toString(),address.text.toString(),email.text.toString(),phoneNumber.text.toString())
            var db = DatabaseHandler(v.context)
            //db.deleteData()
            db.insertData(profile)
        }
    }

    private fun databaseSizeTester(){
        val profileList = databaseHelper.getAllProfiles()
        name.setText(profileList.get(0).name.toString())
        address.setText(profileList.get(0).address.toString())
        email.setText(profileList.get(0).email.toString())
        phoneNumber.setText(profileList.get(0).phoneNumber.toString())
        Log.d("test","${profileList.size}")
    }
}
