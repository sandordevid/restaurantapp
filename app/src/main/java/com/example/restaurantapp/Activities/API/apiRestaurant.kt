package com.example.restaurantapp.Activities.API

import java.io.Serializable

data class RecyclerList(val page: String, val restaurants: ArrayList<apiRestaurant>)
data class Cities(val cities: List<String>)
data class apiRestaurant(
    val address: String,
    val area: String,
    val city: String,
    val country: String,
    val id: Long,
    val image_url: String,
    val lat: Double,
    val lng: Double,
    val mobile_reserve_url: String,
    val name: String,
    val phone: String,
    val postal_code: String,
    val price: Int,
    val reserve_url: String,
    val state: String
) : Serializable

