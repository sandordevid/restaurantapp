package com.example.restaurantapp.Activities.Model

data class Countries (
    val count: Int,
    val countries: List<String>
)